package main;

import imageManagement.ImageAnalyzer;
import genetic.ImageBuilder;
import lib.IConstants;
import ui.MainFrame;
import util.SVGCreator;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

public class Main {

    private static Robot robot;

    private static void changeWindow() throws InterruptedException {
        robot.keyPress(KeyEvent.VK_ALT);
        robot.keyPress(KeyEvent.VK_TAB);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.keyRelease(KeyEvent.VK_TAB);
        Thread.sleep(1000);
    }

    public static void main(String[] args) throws IOException, AWTException, InterruptedException {
        robot = new Robot();
        String path = "assets/guacamaya.png";
        SVGCreator.createEmptySVG();
        SVGCreator.write(IConstants.IMAGE_1_NAME);
        MainFrame frame = new MainFrame(ImageIO.read(new File(path)));
        Thread.sleep(3000);
        ImageAnalyzer analyzer = new ImageAnalyzer(frame);
        Thread.sleep(2000);
        new ImageBuilder(analyzer.getRegions()).generate(10);
        changeWindow();
        path = "assets/KiwiKiwi.jpeg";
        frame.setImage(ImageIO.read(new File(path)));
        Thread.sleep(3000);
        analyzer = new ImageAnalyzer(frame);
        Thread.sleep(2000);
        changeWindow();
        new ImageBuilder(analyzer.getRegions()).generate(10);
        changeWindow();
        path = "assets/beach.png";
        frame.setImage(ImageIO.read(new File(path)));
        Thread.sleep(3000);
        analyzer = new ImageAnalyzer(frame);
        Thread.sleep(2000);
        changeWindow();
        new ImageBuilder(analyzer.getRegions()).generate(10);
    }
}
