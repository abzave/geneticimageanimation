package imageManagement;

public class ColorAnalyzer {
    public static int distanceBetweenColors(int pColor1, int pColor2){
        int deltaBlue = Math.abs(((pColor1)&0xFF) - ((pColor2)&0xFF));
        int deltaGreen = Math.abs(((pColor1>>8)&0xFF) - ((pColor2>>8)&0xFF));
        int deltaRed = Math.abs(((pColor1>>16)&0xFF) - ((pColor2>>16)&0xFF));
        return deltaRed + deltaGreen + deltaBlue;
    }

    public static boolean colorsAreSimilar(int pColor1, int pColor2, int pSimilarityDistance){
        return distanceBetweenColors(pColor1, pColor2) < pSimilarityDistance;
    }
}
