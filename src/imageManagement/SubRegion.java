package imageManagement;

import lib.IConstants;
import util.Point;
import util.SortPointByX;
import util.SortPointByY;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubRegion {

    private static BufferedImage image;
    private ArrayList<Point> points;
    private int color;

    SubRegion(Point pPoint){
        points = new ArrayList<>();
        points.add(pPoint);
        color = pPoint.getColor();
    }

    SubRegion(ArrayList<Point> pPoints){
        this.points = pPoints;
        color = points.get(0).getColor();
    }

    static void setImage(BufferedImage pImage){
        image = pImage;
    }

    public int getColor(){
        return color;
    }

    void addPoint(Point pPoint){
        points.add(pPoint);
        image.setRGB(pPoint.getX(), pPoint.getY(), color);
    }

    public ArrayList<Point> getPoints(){
        return points;
    }

    public ArrayList<SubRegion> split(){
        ArrayList<SubRegion> subRegions = new ArrayList<>();
        Point pivot = points.get(0);
        int currentSubRegionFirstPointIndex = 0;
        for(int pointIndex = 0; pointIndex < points.size(); pointIndex++){
            if(pivot.getX() + IConstants.GAP_RANGE_OF_ACCEPTANCE > points.get(pointIndex).getX()) {
                continue;
            }else if(pivot.getY() + IConstants.MINIMUM_PIXELS_FOR_GAP < points.get(pointIndex).getY()){
                subRegions.add(createNewSubRegion(currentSubRegionFirstPointIndex, pointIndex));
                currentSubRegionFirstPointIndex = pointIndex;
            }
            pivot = points.get(pointIndex);
        }
        subRegions.add(createNewSubRegion(currentSubRegionFirstPointIndex, points.size()));
        return subRegions;
    }

    public void sortPointsByX(){
        Collections.sort(points, new SortPointByX());
    }

    public void sortPointsByY(){
        Collections.sort(points, new SortPointByY());
    }

    private SubRegion createNewSubRegion(int pStartIndex, int pEndIndex){
        List<Point> pointsSubList = points.subList(pStartIndex, pEndIndex);
        ArrayList<Point> newSubRegionPoints = new ArrayList<>(pointsSubList);
        return new SubRegion(newSubRegionPoints);
    }

    public int getMaxX(){
        return points.get(points.size() - 1).getX();
    }

    public int getMinX(){
        return points.get(0).getX();
    }

    public int getMaxY(){
        return points.get(points.size() - 1).getY();
    }

    public int getMinY(){
        return points.get(0).getY();
    }

}