package imageManagement;

import lib.IConstants;
import ui.MainFrame;
import util.Point;
import util.Randoms;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class ImageAnalyzer implements IConstants {

    private MainFrame frame;
    private BufferedImage image;

    private int imageWidth;
    private int imageHeight;
    private int xMovingFactor;
    private int yMovingFactor;
    private ArrayList<Region> regions;
    private int pixelsPerRegion;
    private int regionSize;
    private Point randomPoint;
    private int totalSamples;


    public ImageAnalyzer(MainFrame pFrame){
        frame = pFrame;
        image = frame.getImage();
        imageHeight = image.getHeight();
        imageWidth = image.getWidth();
        regions = new ArrayList<>();
        randomPoint = new Point(0,0);
        SubRegion.setImage(image);
        regionSize = (int)Math.sqrt(NUMBER_OF_REGIONS);
        divideImage();
        pixelsPerRegion = (int)((PERCENTAGE_OF_EXTRACTION/100)*xMovingFactor*yMovingFactor);
        int xLowerLimit = 0;
        int yUpperLimit = 0;
        totalSamples = 0;
        for(int region = 0, row = 1; region < NUMBER_OF_REGIONS; region++, row++) {
            regions.add(new Region(xLowerLimit, yUpperLimit, xLowerLimit+xMovingFactor, yUpperLimit+yMovingFactor));
            xLowerLimit += xMovingFactor;
            if(row == regionSize){
                xLowerLimit = 0;
                yUpperLimit += yMovingFactor;
                row = 0;
            }
        }
        extractSamples();
        divideRegions();
        splitSubRegions();
        frame.repaint();
    }

    private void divideRegions() {
        for(Region region : regions){
            for(int rows = region.getxLowerLimit(); rows < region.getxUpperLimit(); rows++) {
                image.setRGB(rows, region.getyLowerLimit(), BLACK);
                image.setRGB(rows, region.getyUpperLimit(), BLACK);
            }
            for(int columns = region.getyUpperLimit(); columns < region.getyLowerLimit(); columns++) {
                image.setRGB(region.getxLowerLimit(), columns, BLACK);
                image.setRGB(region.getxUpperLimit(), columns, BLACK);
            }
        }
    }

    private void divideImage() {
        xMovingFactor = imageWidth/regionSize-1;
        yMovingFactor = imageHeight/regionSize-1;
    }


    private void atemptSampling(Region pRegion){
        if(Randoms.randInt(0, pRegion.getProbability()) > 0){
            int probability = pRegion.getProbability();
            if(addRandomValidPixel(pRegion)){
                probability += probability/TRUSTING_PIXEL_FACTOR;
                probability = Math.min(probability, 100);
            }else{
                probability -= probability/DISTRUSTING_PIXEL_FACTOR+1;
            }
            pRegion.setProbability(probability);
        }
    }

    private void extractSamples() {
        int totalSamplings = pixelsPerRegion;
        for(int actualSample = 0; actualSample < totalSamplings; actualSample++) {
            for(int region = 0; region < NUMBER_OF_REGIONS; region++){
                atemptSampling(regions.get(region));
            }
        }

    }

    private void fillSubRegions(Region pRegion, Point pPoint){
        ArrayList<SubRegion> subRegions = pRegion.getSubRegions();
        int pointColor = pPoint.getColor();
        for(SubRegion subRegion : subRegions){
            if(ColorAnalyzer.colorsAreSimilar(subRegion.getColor(), pointColor, COLOR_SIMILARITY_FOR_SUB_REGIONS)){
                subRegion.addPoint(pPoint);
                return;
            }
        }
        subRegions.add(new SubRegion(pPoint));
    }

    private boolean addRandomValidPixel(Region pRegion) {
        randomPoint.setCoordinates(Randoms.randInt(pRegion.getxLowerLimit(),pRegion.getxUpperLimit()),
                Randoms.randInt(pRegion.getyUpperLimit(),pRegion.getyLowerLimit()));
        int color = image.getRGB(randomPoint.getX(), randomPoint.getY());
        if(ColorAnalyzer.colorsAreSimilar(color, WHITE, COLOR_SIMILARITY_FOR_SAMPLING)){
            return false;
        }
        totalSamples++;
        fillSubRegions(pRegion, new Point(randomPoint.getX(), randomPoint.getY(), color));
        return true;
    }

    public ArrayList<Region> getRegions(){
        return regions;
    }

    private void splitSubRegions(){
        ArrayList<SubRegion> subRegionsToAdd;
        for(Region region : regions){
            subRegionsToAdd = new ArrayList<>();
            for(SubRegion subRegion : region.getSubRegions()){
                subRegion.sortPointsByX();
                subRegionsToAdd.addAll(subRegion.split());
            }
            region.setSubRegions(subRegionsToAdd);
        }
    }

    public int getTotalSamples(){
        return totalSamples;
    }

}
