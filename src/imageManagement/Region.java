package imageManagement;

import java.util.ArrayList;

public class Region {

    private ArrayList<SubRegion> subRegions;
    private int probability;
    private int xLowerLimit;
    private int yLowerLimit;
    private int xUpperLimit;
    private int yUpperLimit;

    public Region(int pXLowerLimit, int pYUpperLimit, int pXUpperLimit, int pYLowerLimit){
        subRegions = new ArrayList<>();
        probability = 100;
        xLowerLimit = pXLowerLimit;
        yLowerLimit = pYLowerLimit;
        xUpperLimit = pXUpperLimit;
        yUpperLimit = pYUpperLimit;
    }

    public ArrayList<SubRegion> getSubRegions(){
        return subRegions;
    }

    public int getProbability() {
        return probability;
    }

    public void setProbability(int pProbability) {
        this.probability = pProbability;
    }

    public int getxLowerLimit() {
        return xLowerLimit;
    }

    public int getyLowerLimit() {
        return yLowerLimit;
    }

    public int getxUpperLimit() {
        return xUpperLimit;
    }

    public int getyUpperLimit() {
        return yUpperLimit;
    }

    @Override
    public String toString() {
        return "Region{" +
                "xLowerLimit=" + xLowerLimit +
                ", yLowerLimit=" + yLowerLimit +
                ", xUpperLimit=" + xUpperLimit +
                ", yUpperLimit=" + yUpperLimit +
                '}';
    }

    public void setSubRegions(ArrayList<SubRegion> pSubRegions){
        this.subRegions = pSubRegions;
    }

    public int getSamples(){
        int samples = 0;
        for (SubRegion subRegion: subRegions) {
            samples += subRegion.getPoints().size();
        }
        return samples;
    }

}
