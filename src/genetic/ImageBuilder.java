package genetic;

import imageManagement.Region;
import imageManagement.SubRegion;
import lib.IConstants;
import util.Point;
import util.Randoms;
import util.SVGCreator;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.time.temporal.ValueRange;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class ImageBuilder {

    private ArrayList<Region> regions;
    private ArrayList<HashMap<SubRegion, Double>> distributionTable;
    private ArrayList<HashMap<ValueRange, SubRegion>> chromosomeTable;
    private Robot robot;

    public ImageBuilder(ArrayList<Region> pRegions) throws AWTException {
        regions = pRegions;
        discardEmptyRegions();
        calculateSubRegionDistribution();
        calculateChromosomalDistribution();
        robot = new Robot();
    }

    private void discardEmptyRegions(){
        ArrayList<Region> regionsToDiscard = new ArrayList<>();
        for(Region region : regions){
            if(region.getProbability() == 0){
                regionsToDiscard.add(region);
            }
        }
        for(Region region : regionsToDiscard){
            regions.remove(region);
        }
    }

    private void calculateSubRegionDistribution(){
        distributionTable = new ArrayList<>();
        for(Region region : regions){
            HashMap<SubRegion, Double> subRegionDistribution = new HashMap<>();
            for(SubRegion subRegion : region.getSubRegions()){
                double populationPercentage = (double)subRegion.getPoints().size() / region.getSamples();
                subRegionDistribution.put(subRegion, populationPercentage);
            }
            distributionTable.add(subRegionDistribution);
        }
    }

    private void calculateChromosomalDistribution(){
        chromosomeTable = new ArrayList<>();
        for(int regionNuber = 0; regionNuber < regions.size(); regionNuber++) {
            long lastChromosomeValue = 0;
            HashMap<SubRegion, Double> subRegionDistribution = distributionTable.get(regionNuber);
            HashMap<ValueRange, SubRegion> subRegionChromosomes = new HashMap<>();
            for (SubRegion subRegion : subRegionDistribution.keySet()) {
                double distibution = subRegionDistribution.get(subRegion);
                double values = IConstants.CHROMOSOMES_VALUES * distibution;
                long lastValue = lastChromosomeValue + (long) values;
                subRegionChromosomes.put(ValueRange.of(lastChromosomeValue, lastValue), subRegion);
                lastChromosomeValue = lastValue + 1;
            }
            chromosomeTable.add(subRegionChromosomes);
        }
    }

    private ArrayList<ArrayList<Long>> generatePopulations(int pAmount){
        ArrayList<ArrayList<Long>> populations = new ArrayList<>();
        for(Region region : regions){
            ArrayList<Long> regionPopulation = new ArrayList<>();
            for(int generatedAmount = 0; generatedAmount < pAmount; generatedAmount++){
                regionPopulation.add(Randoms.randLong(0, IConstants.CHROMOSOMES_VALUES));
            }
            populations.add(regionPopulation);
        }
        return populations;
    }

    private boolean fitness(Long individual, int region, ArrayList<Long> population){
        ArrayList<Long> similarIndividuals = getSimilarIndividuals(individual, region, population);
        double currentPercentage = (double) similarIndividuals.size() / population.size();
        double targetPercentage = getTargetPercentage(individual, region);
        return currentPercentage < targetPercentage * IConstants.ACCEPTABILITY_PERCENTAGE;
    }

    private double getTargetPercentage(long individual, int region){
        ValueRange range = getIndividualRange(individual, region);
        SubRegion subRegion = chromosomeTable.get(region).get(range);
        return distributionTable.get(region).get(subRegion);
    }

    private ArrayList<Long> getSimilarIndividuals(Long individual, int region, ArrayList<Long> population){
        ValueRange range = getIndividualRange(individual, region);
        ArrayList<Long> similarIndividuals = new ArrayList<>();
        for(Long currentIndividual : population){
            if(range.isValidValue(currentIndividual) && !currentIndividual.equals(individual)){
                similarIndividuals.add(currentIndividual);
            }
        }
        return similarIndividuals;
    }

    private ValueRange getIndividualRange(long indivual, int region){
        for(ValueRange range : chromosomeTable.get(region).keySet()){
            if(range.isValidValue(indivual)){
                return range;
            }
        }
        return null;
    }

    private Long mutate(Long individual){
        Random random = new Random();
        if(random.nextDouble() <= IConstants.MUTATION_PERCENTAGE){
            long bitMask = IConstants.BIT_MASK;
            int shiftAmount = Randoms.randInt(0, IConstants.CHROMOSOMES_BITS - 1);
            bitMask <<= shiftAmount;
            long bitValue = individual & bitMask;
            bitValue >>= shiftAmount;
            if(bitValue == 1) {
                bitMask = IConstants.CHROMOSOME_MAX_VALUE;
                bitMask -= Math.pow(2, shiftAmount);
                individual &= bitMask;
            }else{
                bitMask = IConstants.BIT_MASK;
                bitMask <<= shiftAmount;
                individual |= bitMask;
            }
        }
        return individual;
    }

    public void generate(int epochs) throws IOException, InterruptedException {
        ArrayList<ArrayList<Long>> populations = generatePopulations(IConstants.INITIAL_POPULATION_SIZE);
        ArrayList<Long> fitIndividuals = new ArrayList<>();
        File htmlFile = new File("html/canvas.html");
        Desktop.getDesktop().browse(htmlFile.toURI());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for(int epoch =  0; epoch < epochs; epoch++){
            for(int region = 0; region < populations.size(); region++){
                ArrayList<Long> regionPopulation = new ArrayList<>(populations.get(region));
                for(Long indivual : regionPopulation){
                    if(fitness(indivual, region, populations.get(region))){
                        fitIndividuals.add(indivual);
                    }
                    populations.get(region).remove(indivual);
                }
                int populationSize = 2 * regionPopulation.size();
                populations.get(region).clear();
                for(int currentPopulationSize = 0; currentPopulationSize < populationSize; currentPopulationSize++){
                    int firstParentIndex = Randoms.randInt(0, fitIndividuals.size() - 1);
                    int secondParentIndex = Randoms.randInt(0, fitIndividuals.size() - 1);
                    Long firstParent = fitIndividuals.get(firstParentIndex);
                    Long secondParent = fitIndividuals.get(secondParentIndex);
                    Long child = intersect(firstParent, secondParent);
                    populations.get(region).add(mutate(child));
                }
                fitIndividuals.clear();
            }
            createSVG(populations, 50 * epoch);
            refreshPage();
        }
        refreshPage();
        Thread.sleep(5000);
        SVGCreator.createEmptySVG();
        SVGCreator.write(IConstants.IMAGE_1_NAME);
    }

    private void refreshPage(){
        robot.keyPress(KeyEvent.VK_F5);
        robot.keyRelease(KeyEvent.VK_F5);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Long intersect(Long firstParent, Long secondParent) {
        int intersectionPoint = Randoms.randInt(0, IConstants.CHROMOSOMES_BITS);
        long bitMask = IConstants.CHROMOSOME_MAX_VALUE;
        long child;
        long highBits;
        bitMask >>>= IConstants.CHROMOSOMES_BITS - intersectionPoint;
        child = firstParent & bitMask;
        highBits = secondParent | bitMask;
        highBits >>>= IConstants.CHROMOSOMES_BITS - intersectionPoint;
        highBits <<= IConstants.CHROMOSOMES_BITS - intersectionPoint;
        child |= highBits;
        return child;
    }

    private void createSVG(ArrayList<ArrayList<Long>> population, int generation){
        ArrayList<ArrayList<Point>> points = translatePopulationToPolygonPoints(population, generation);
        ArrayList<String> colors = translatePopulationToFill(population);
        SVGCreator.createEmptySVG();
        SVGCreator.addPolygons(points, colors);
        SVGCreator.write(IConstants.IMAGE_1_NAME);
    }

    private ArrayList<ArrayList<Point>> translatePopulationToPolygonPoints(ArrayList<ArrayList<Long>> population,
                                                                           int offset){
        ArrayList<ArrayList<Point>> points = new ArrayList<>();
        for(int region = 0; region < population.size(); region++){
            for(Long individual : population.get(region)){
                ArrayList<Point> vertices = new ArrayList<>();
                Point pivot = translateIndividualToVertex(individual, region);
                pivot.move(offset);
                int rightVertexX = pivot.getX() + IConstants.POLYGON_WIDTH;
                int downVertexY = pivot.getY() + IConstants.POLYGON_HEIGHT;
                Point rightVertex = new Point(rightVertexX, pivot.getY(), pivot.getColor());
                Point downVertex = new Point(rightVertexX, downVertexY, pivot.getColor());
                Point leftVertex = new Point(pivot.getX(), downVertexY, pivot.getColor());
                vertices.add(pivot);
                vertices.add(rightVertex);
                vertices.add(downVertex);
                vertices.add(leftVertex);
                points.add(vertices);
            }
        }
        return points;
    }

    private Point translateIndividualToVertex(long individual, int region){
        ValueRange range = getIndividualRange(individual, region);
        SubRegion subRegion = chromosomeTable.get(region).get(range);
        subRegion.sortPointsByX();
        int individualX = getPointCoordinate(individual, subRegion, IConstants.GET_X);
        subRegion.sortPointsByY();
        int individualY = getPointCoordinate(individual, subRegion, IConstants.GET_Y);
        return new Point(individualX, individualY, IConstants.WHITE);
    }

    private int getPointCoordinate(long individual, SubRegion subRegion, boolean getX){
        int max = getX ? subRegion.getMaxX() : subRegion.getMaxY();
        int min = getX ? subRegion.getMinX() : subRegion.getMinY();
        int bits = (int)individual & (getX ? IConstants.X_MASK : IConstants.Y_MASK);
        if(!getX){
            bits >>>= IConstants.Y_BIT_OFFSET;
        }
        double position = bits / (double)IConstants.X_AND_Y_VALUES;
        int distance = max - min;
        double offset = distance * position;
        return (int)(min + offset);
    }

    private ArrayList<String> translatePopulationToFill(ArrayList<ArrayList<Long>> population){
        ArrayList<String> colors = new ArrayList<>();
        for(int region = 0; region < population.size(); region++){
            for(Long individual : population.get(region)){
                colors.add(translateIndividualToColor(individual, region));
            }
        }
        return colors;
    }

    private String translateIndividualToColor(Long individual, int region){
        ValueRange range = getIndividualRange(individual, region);
        SubRegion subRegion = chromosomeTable.get(region).get(range);
        return IConstants.COLOR_HASHTAG + Integer.toHexString(subRegion.getColor() & IConstants.WHITE);
    }

}
