package ui;

import lib.IConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class MainFrame extends JFrame implements IConstants {

    private BufferedImage image;
    private Graphics graphics;

    public MainFrame(BufferedImage pImage){
        setTitle(windowTitle);
        setImage(pImage);
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public void setImage(BufferedImage pImage) {
        image = pImage;
        graphics = pImage.getGraphics();
        setSize(image.getWidth(), image.getHeight()+getInsets().top);
        setLocationRelativeTo(null);
        repaint();
    }

    public BufferedImage getImage() {
        return image;
    }

    public void paint(Graphics g) {
        g.drawImage(image, getInsets().right, getInsets().top, this);
    }

}