package lib;

public interface IConstants {
    String windowTitle = "Image display";
    int NUMBER_OF_REGIONS = 64;
    float PERCENTAGE_OF_EXTRACTION = 30;
    int COLOR_SIMILARITY_FOR_SAMPLING = 5;
    int COLOR_SIMILARITY_FOR_SUB_REGIONS = 100;
    int TRUSTING_PIXEL_FACTOR = 2;
    int DISTRUSTING_PIXEL_FACTOR = 42;
    int BLACK = 0;
    int WHITE = 16777215;
    int MAXIMUM_COLOR_VALUE = 0xFFFFFF;
    int MINIMUM_PIXELS_FOR_GAP = 1;
    int GAP_RANGE_OF_ACCEPTANCE = 1;
    long CHROMOSOMES_VALUES = 4294967296L;
    String STROKE_WIDTH = "1";
    double ACCEPTABILITY_PERCENTAGE = 0.95;
    double MUTATION_PERCENTAGE = 0.05;
    int CHROMOSOMES_BITS = 32;
    long BIT_MASK = 0x1;
    long CHROMOSOME_MAX_VALUE = 4294967295L;
    int INITIAL_POPULATION_SIZE = 2;
    int POLYGON_WIDTH = 15;
    int POLYGON_HEIGHT = 15;
    int X_MASK = 127;
    int Y_MASK = 16256;
    int Y_BIT_OFFSET = 7;
    int X_AND_Y_VALUES = 127;
    boolean GET_X = true;
    boolean GET_Y = false;
    int SVG_WIDTH = 800;
    int SVG_HEIGHT = 600;
    String COLOR_HASHTAG = "#";
    boolean APPEND = false;
    String SVG_PATH = "html/";
    String IMAGE_1_NAME = "image1.svg";
}
