package util;

import lib.IConstants;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class SVGCreator {

    private static String startTag = "<svg ";
    private static String svgInfo = "xmlns=\"http://www.w3.org/2000/svg\"";
    private static String svgPoints = "<polygon points=\"";
    private static String svgFill = "\" style=\"fill:";
    private static String svgStroke = ";stroke:";
    private static String svgStrokeWidth = ";stroke-width:";
    private static String svgEnd = "</svg>";
    private static String closeTag = ">";
    private static String closePolygonTag = "/>";
    private static String quote = "\"";
    private  static String comma = ", ";
    private static StringBuilder svg = new StringBuilder(startTag + closeTag + svgEnd);

    public static String createEmptySVG(){
        svg = new StringBuilder(startTag + closeTag + svgEnd);
        int startTagIndex = svg.indexOf(startTag) + startTag.length();
        svg.insert(startTagIndex, svgInfo);
        return svg.toString();
    }

    public static String addPolygons(ArrayList<ArrayList<Point>> pPoints, ArrayList<String> pFills){
        int endSVGTag = svg.indexOf(closeTag) + closeTag.length();
        StringBuilder tags = new StringBuilder();
        for (int polygon = 0; polygon < pPoints.size(); polygon++){
            tags.append(svgPoints);
            tags.append(addPoints(pPoints.get(polygon)));
            tags.append(svgFill);
            tags.append(pFills.get(polygon));
            tags.append(svgStroke);
            tags.append(pFills.get(polygon));
            tags.append(svgStrokeWidth);
            tags.append(IConstants.STROKE_WIDTH);
            tags.append(quote).append(closePolygonTag);
        }
        svg.insert(endSVGTag, tags.toString());
        return svg.toString();
    }

    private static String addPoints(ArrayList<Point> pPoints){
        StringBuilder result = new StringBuilder();
        for(Point point : pPoints){
            result.append(point.getX()).append(comma).append(point.getY()).append(" ");
        }
        return result.toString();
    }

    public static void write(String name){
        String relativePath = IConstants.SVG_PATH + name;
        File file = new File(relativePath);
        try {
            FileWriter fr = new FileWriter(file, IConstants.APPEND);
            fr.write(svg.toString());
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
