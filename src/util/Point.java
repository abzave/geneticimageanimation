package util;

import java.util.Comparator;

public class Point {
    private int x;
    private int y;
    private int color;


    public Point(int pX, int pY){
        setCoordinates(pX, pY);
    }

    public Point(int pX, int pY, int pColor){
        setCoordinates(pX, pY);
        color = pColor;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }


    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void move(int pOffset){
        x += pOffset;
    }

    public void setCoordinates(int pX, int pY) {
        x = pX;
        y = pY;
    }
}
