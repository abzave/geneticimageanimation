package util;

import java.util.Random;

public class Randoms {

    private static Random random = new Random();

    public static int randInt(int pMin, int pMax) {
        return random.nextInt((pMax - pMin) + 1) + pMin;
    }

    public static double randDouble(double pMin, double pMax){
        return  pMin + Math.random() * (pMax - pMin);
    }

    public static long randLong(long pMin, long pMax){
        return (long)(pMin + Math.random() * (pMax - pMin));
    }

}
